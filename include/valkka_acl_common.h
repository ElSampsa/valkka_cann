#ifndef example_common_HEADER_GUARD
#define example_common_HEADER_GUARD
/*
 * valkka_acl_common.h : CANN accelerated decoding for libValkka - common headers files
 * 
 * Authors:
 * 
 * (c) Copyright 2021
 * 
 *            DO WHAT THE FUCK YOU WANT TO PUBLIC LICENSE 
 *   TERMS AND CONDITIONS FOR COPYING, DISTRIBUTION AND MODIFICATION 
 * 
 *  0. You just DO WHAT THE FUCK YOU WANT TO. 
 * 
 */

/** 
 *  @file    valkka_acl_common.h
 *  @author
 *  @date    2021
 *  @version 1.0.0 
 *  
 *  @brief
 */ 

// libValkka headers
#include "framefilter.h"
#include "framefifo.h"
#include "thread.h"
#include "decoderthread.h"
#include "decoder.h"

// system headers
#include <assert.h>
#include <stdint.h>
#include <mutex>
#include <vector>
#include <string>
#include <sstream>
#include <string.h>
#include <iostream>
#include <algorithm>
#include <chrono>

// libACL / CANN headers
#ifdef NO_ACL
#else
#include "acl/acl.h"
#include "acl/ops/acl_dvpp.h"
#endif

#endif
