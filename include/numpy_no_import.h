#ifndef numpy_no_import_HEADER_GUARD
#define numpy_no_import_HEADER_GUARD
/*
 * numpy_no_import.h :
 * 
 * Copyright 2017-2020 Valkka Security Ltd. and Sampsa Riikonen
 * 
 * Authors: Sampsa Riikonen <sampsa.riikonen@iki.fi>
 * 
 * This file is part of the Valkka library.
 * 
 * Valkka is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>
 *
 */

/** 
 *  @file    numpy_no_import.h
 *  @author  Sampsa Riikonen
 *  @date    2017
 *  @version 1.1.0 
 *  
 *  @brief
 */ 

// https://github.com/numpy/numpy/issues/9309
// https://numpy.org/doc/stable/reference/c-api/array.html#importing-the-api

#define NPY_NO_DEPRECATED_API NPY_1_7_API_VERSION
// #define PY_ARRAY_UNIQUE_SYMBOL py_acl_ARRAY_API // KEEP THIS COMMENTED: we use the definition that comes from libValkka
#define NO_IMPORT_ARRAY // avoid running import_array() in these include files: (it will be run by swig)
#include "numpy/ndarraytypes.h"
#include "numpy/arrayobject.h"

#endif
