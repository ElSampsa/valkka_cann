#ifndef acldecoder_HEADER_GUARD
#define acldecoder_HEADER_GUARD
/*
 * acldecoder.h : CANN accelerated decoder class for libValkka
 * 
 * Authors: XXX
 * 
 * (c) Copyright 2021 XXX
 * 
 *            DO WHAT THE FUCK YOU WANT TO PUBLIC LICENSE 
 *   TERMS AND CONDITIONS FOR COPYING, DISTRIBUTION AND MODIFICATION 
 * 
 *  0. You just DO WHAT THE FUCK YOU WANT TO. 
 * 
 */

/** 
 *  @file    acldecoder.h
 *  @author
 *  @date    2021
 *  @version 1.0.0 
 *  
 *  @brief   CANN accelerated decoder class for libValkka
 */ 

#include "valkka_acl_common.h"
#include "semaring.h"

void acl_decoder_callback(
    acldvppStreamDesc* input, 
    acldvppPicDesc* output, 
    void* userData);

/**How decoder thread uses the decoder API
 * 
 * - ACLThread populates member ACLDecoder::in_frame
 * - ACLThread calls method ACLDecoder::pull
 *      - ACLDecoder::pull uses ACLDecoder::in_frame and aclvdecSendFrame 
 *        & sends the data for decoding
 *      - Frame is decoded asynchronously, see ACLDecoder::callback
 *      - ACLDecoder::callback places the result into ringbuffer ACLDecoder::out_frame_rb
 * - ACLDecoder::pull inspects the ringbuffer.  If there is an available frame,
 *   returns true, otherwise false
 * - ACLThread calls immediately ACLDecoder::output
 * - ACLDecoder::output returns the correct out_frame (AVBitmapFrame) from the ringbuffer
 * 
 */
class ACLDecoder : public Decoder {

public:
    ACLDecoder(AVCodecID av_codec_id, int32_t device_id=0, int n_buf=5);
    virtual ~ACLDecoder();
    static void* threadfunc(void *arg); ///< A thread function required by aclvdecChannelDesc
    aclrtContext getContext() { return acl_context; }
    std::mutex threadfunc_loop_mutex;
    bool threadfunc_loop;
public:
    AVCodecID av_codec_id;  ///< FFmpeg AVCodecId, identifying the codec

protected:
    AVBitmapFrame out_frame;
    bool    active;

protected: // libACL stuff
    int32_t                 device_id;
    uint32_t                channel_id;
    // uint64_t                thread_id;
    aclvdecFrameConfig*     frame_config;
    aclvdecChannelDesc*     channel_desc;
    acldvppStreamFormat     en_type;
    acldvppStreamDesc*      stream_desc;
    acldvppPicDesc*         pic_desc;
    void*                   dev_ptr;
    void*                   packet_mem_ptr;
    size_t                  packet_mem_size;
    aclrtStream             acl_stream = nullptr; ///< Given by the decoder thread
    aclrtContext            acl_context = nullptr;

private:
    pthread_t   thread_id;
    SemaRingBuffer  semaring;
    std::mutex      mutex;
    std::vector<AVBitmapFrame*> out_frame_rb;
    int         current_width;
    int         current_height;

public:
    void callback(acldvppStreamDesc * input, acldvppPicDesc * output);


// There is a (not-so-well-documented) API between decoding threads and decoders
// The ACLThread uses this decoder & it's public methods
public:
    virtual Frame *output();            ///< Returns a pointer to the current Frame object
    virtual void flush();               ///< Clears the decoder state
    /** pull(): Decodes a frame 
     * 
     * Please see the mother class definition here: 
     * https://elsampsa.github.io/valkka-core/html/classDecoder.html
     * 
     * - inspects member ACLDecoder::in_frame
     * - sends data in that member for decoding
     * - if there is a frame available immediately, return true
     * - ..otherwise return false
     * - member ACLDecoder::output returns pointer to the decoded frame
     *   (which is of the type AVBitmapFrame)
     * 
     */
    virtual bool pull();              
    virtual void releaseOutput();
    virtual bool isOk();
    void deactivate(const char* err);
};


#endif
