#ifndef aclthread_HEADER_GUARD
#define aclthread_HEADER_GUARD
/*
 * aclthread.h : Cuda accelerated decoder thread for CANN
 * 
 * Authors:
 * 
 * (c) Copyright 2021
 * 
 *            DO WHAT THE FUCK YOU WANT TO PUBLIC LICENSE 
 *   TERMS AND CONDITIONS FOR COPYING, DISTRIBUTION AND MODIFICATION 
 * 
 *  0. You just DO WHAT THE FUCK YOU WANT TO. 
 * 
 */

/** 
 *  @file    aclthread.h
 *  @author  
 *  @date    2021
 *  @version 1.0.0 
 *  
 *  @brief   Cuda accelerated decoder thread for CANN
 */ 

#include "valkka_acl_common.h"

class ACLThread : public DecoderThread { // <pyapi>
  
public: // <pyapi>
    /** Default constructor
    * 
    * @param name              Name of the thread
    * @param outfilter         Outgoing frames are written here.  Outgoing frames may be of type FrameType::avframe
    * @param fifo_ctx          Parametrization of the internal FrameFifo
    * 
    */
    ACLThread(const char* name, FrameFilter& outfilter, FrameFifoContext fifo_ctx=FrameFifoContext());   // <pyapi>
    virtual ~ACLThread(); ///< Default destructor.  Calls AVThread::stopCall                             // <pyapi>

protected:
    virtual Decoder* chooseAudioDecoder(AVCodecID codec_id);
    virtual Decoder* chooseVideoDecoder(AVCodecID codec_id);
    virtual Decoder* fallbackAudioDecoder(AVCodecID codec_id);
    virtual Decoder* fallbackVideoDecoder(AVCodecID codec_id);

private:
    /**ACL stream can be created as per ACLThread (decoding thread)
    Or it could be passed to the ACLThread as a parameter
    (if you want to use the same acl stream for a group of decoders)
    */
    // aclrtStream* acl_stream;

}; // <pyapi>


/** Dumps AVBitmapFrame(s) into a files
 * @ingroup filters_tag
 */
class DumpAVBitmapFrameFilter : public FrameFilter { // <pyapi>

public:                                                          // <pyapi>
    DumpAVBitmapFrameFilter(const char *name, FrameFilter *next = NULL); // <pyapi>

protected:
    int count;

protected:
    void go(Frame *frame);
}; // <pyapi>


#endif
