#ifndef aclpredictor_HEADER_GUARD
#define aclpredictor_HEADER_GUARD
/*
 * aclpredictor.h : Python bindings for libACL/Ascend inference
 * 
 * Authors: Sampsa Riikonen <sampsa.riikonen@silo.ai>
 * 
 */

/** 
 *  @file    aclpredictor.h
 *  @author  Sampsa Riikonen
 *  @date    2020
 *  @version 0.0.1
 *  
 *  @brief   Python bindings for libACL/Ascend inference
 */ 

#include "valkka_acl_common.h"


#include <Python.h>


// bool verbose = false;
bool verbose = true;


/**A base/dummy test class for a python-wrapped predictor
 * 
 * predict takes as an input a list of 1D numpy byte buffers
 * 
 * getArrayList returns a list of numpy output arrays (again, 1D numpy byte buffers)
 */
class PythonPredictor {                                                 // <pyapi>    
public:                                                                 // <pyapi>
    PythonPredictor();                                                  // <pyapi>
    virtual ~PythonPredictor();                                         // <pyapi>

protected:
    PyObject* python_list;
    std::vector<PyObject*> python_arrays;
    std::vector<uint8_t*> output_byte_buffers;
    bool first_exec;

protected:
    virtual void freeDataBuffers();

public:                                                                 // <pyapi>
    PyObject* getArrayList();                                           // <pyapi>
    virtual PyObject* predict(PyObject* plis);                          // <pyapi>
};                                                                      // <pyapi>

#ifdef NO_ACL                                                           // <pyapi>
#else                                                                   // <pyapi>
/**A general-purpose ACL neural net inference for both python and c++

It is up to the end-user to create correct input byte-buffer(s) for the model in question
and to create meaningfull data from the output byte buffer(s).

TODO: create a version where context+stream pair can be shared among different
predictors
*/
class ACLPredictor : public PythonPredictor {                           // <pyapi>
                                                                        // <pyapi>
public:                                                                 // <pyapi>
    ACLPredictor(const char *om_file);                                 // <pyapi>
    virtual ~ACLPredictor();                                            // <pyapi>
                                                            
protected: 
    std::string om_file; //, acl_file;
    bool activated;
    /**A record of aclDataBuffer(s) for the data input

    The logic of the memory management is like this:

    [aclmdlDataset [aclDataBuffer [reserved memaddr]] [aclDataBuffer [reserved memaddr]]]

    - Create aclmdlDataset
    - Reserve memory on-device for the aclDataBuffer ("reserved memaddr") with "aclrtMalloc"
    - Associate that memory to a new aclDataBuffer with "aclCreateDataBuffer"
    - Attach the aclDataBuffer to a aclmdDataset with "aclmdlAddDatasetBuffer"
    - Copying data to/from the device is done with "aclrtMemcpy".  The pointer to be used is
    the "reserved memaddr".
    */
    std::vector<aclDataBuffer*> acl_input_data_buffers;
    std::vector<aclDataBuffer*> acl_output_data_buffers;

private: // taken from object_detect.h
    int32_t deviceId_;
    // bool acl_init;
    bool device_set;
    aclrtContext* context_;
    aclrtStream* stream_;
    aclrtRunMode runMode_;

private: // from model_process.h
    uint32_t modelId_;
    void *modelMemPtr_;
    size_t modelMemSize_;
    void *modelWeightPtr_;
    size_t modelWeightSize_;
    aclmdlDesc *modelDesc_;
    aclmdlDataset *input_;
    aclmdlDataset *output_;

protected:
    virtual void freeDataBuffers();
    void release();

public:                                                                // <pyapi>
    virtual PyObject* predict(PyObject* plis);                         // <pyapi>
    PyObject* getInputSizes();                                         // <pyapi>
};                                                                     // <pyapi>
#endif                                                                 // <pyapi>

#endif


