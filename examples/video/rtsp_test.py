"""taken from here: https://elsampsa.github.io/valkka-examples/_build/html/lesson_2.html

Usage:

::

    python3 rtsp_test.py rtsp://username:passwd@ip-address

filtergraph:

Streaming part               | Decoding part
                             |
(LiveThread:livethread) -->> (AVThread:avthread) --> {InfoFrameFilter:info_filter}
"""
import time, sys

rtsp_adr = sys.argv[1]

from valkka.core import *
import valkka.acl
from valkka.acl.core import ACLThread

# decoding part
info_filter     =InfoFrameFilter("info_filter")
# avthread        =AVThread("avthread",info_filter)
# Substituting AVThread with ACLThread
avthread        =ACLThread("avthread",info_filter)

# streaming part
av_in_filter    =avthread.getFrameFilter()
livethread      =LiveThread("livethread")

ctx =LiveConnectionContext(LiveConnectionType_rtsp, rtsp_adr, 1, av_in_filter)

"""#dummy testing if we get some numpy conflict or not..
from valkka.api2 import ShmemRGBClient
shmem_filter=RGBShmemFrameFilter("kokkelis", 10, 1000, 1000)
client = ShmemRGBClient(
    name="kokkelis",
    n_ringbuffer=10,
    width=1000,
    height=1000,
    mstimeout=1000,        # client timeouts if nothing has been received in 1000 milliseconds
    verbose=False
)
# seems to work
"""

avthread.startCall()
livethread.startCall()

# start decoding
avthread.decodingOnCall()

livethread.registerStreamCall(ctx)
livethread.playStreamCall(ctx)
time.sleep(5)

# stop decoding
# avthread.decodingOffCall()

livethread.stopCall()
avthread.stopCall()

print("bye")
