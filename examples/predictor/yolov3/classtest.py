import sys, os
import numpy as np
import cv2
import valkka.acl # initializes ACL
from valkka.acl.api2 import ACLPredictor
from valkka.acl.tools.coco14 import labels as coco_labels

class YoloV3Detector(ACLPredictor):
    """YoloV3 detector
    
    Input: RGB24 Image

    Output: List of tuples that look like this: (x1, y1, x2, y2, score, label)

    On device failure, returns None
    """
    def __call__(self, img):
        if not self.ok:
            print(self.name, ": was not initialized properly")
            return None
        # *** TRANSFORM HERE YOUR INPUT INTO A LIST OF 1D BYTEBUFFERS ***
        # input buffers: the image
        # four single precision floats:
        # the floats represent model input width, height, original image width height
        orig_size = img.shape
        size = (416, 416) # target input size of the image for this particular neural net

        # img = cv2.resize(img, (416, 416))
        ## instead of doing just that, padding is added to
        ## the image for correct aspect ration (for some reason?)
        h, w = img.shape[:2]
        c = img.shape[2] if len(img.shape) > 2 else 1
        if h == w: 
            img = cv2.resize(img, size, cv2.INTER_AREA)
        dif = h if h > w else w
        interpolation = cv2.INTER_AREA if dif > (size[0]+size[1])//2 else \
                        cv2.INTER_CUBIC
        x_pos = (dif - w)//2
        y_pos = (dif - h)//2        
        mask = np.zeros((dif, dif, c), dtype=img.dtype)
        mask.fill(128) # add grey padding margins.  hmm..?  why grey?
        mask[y_pos:y_pos+h, x_pos:x_pos+w, :] = img[:h, :w, :]
        img = cv2.resize(mask, size, interpolation)

        # convert to 1D byte array for input
        img = img.flatten()

        # these are the buffers required by this particular mode
        # why we need "original width & height", don't ask me
        input_buffer_list = [
            img,                        # image
            np.frombuffer(np.array([    # the floats
                size[0], size[1], orig_size[0], orig_size[1]
                # model input width, height, orig width, height
            ],dtype=np.float32).tobytes(), dtype=np.uint8)
        ]
        # ***************************************************************
        success = self.core.predict(input_buffer_list)

        if not success:
            print(self.name, ": failed!")
            return None

        # if success is True, it means that self.output_buffer_list has
        # an updated list of byte buffers

        # *** TRANSFORM HERE YOUR OUTPUT BUFFER LIST ********************
        # output buffers of this particular neural net:
        # - Buffer 1 : 24576 bytes => 6144 float32s
        # - Buffer 2 : 32 bytes => 8 uint32s
        buf1 = self.output_buffer_list[0] # bboxes 
        buf2 = self.output_buffer_list[1] # number of detections

        nums = np.frombuffer(buf2, dtype=np.uint32) # bytes to uint32 => n = 8
        detections = np.frombuffer(buf1, dtype=np.float32) # bytes to float32 => n = 6144 

        results = []
        n = nums[0]
        for i in range(n):
            x1 = int(detections[0 * n + i])
            y1 = int(detections[1 * n + i])
            x2 = int(detections[2 * n + i])
            y2 = int(detections[3 * n + i])
            score = float(detections[4 * n + i])
            n_label = int(float(detections[5 * n + i])) + 1 # +1 : see py_ascend.tools.coco14
            label = coco_labels[n_label]
            results.append((
                x1, y1, x2, y2, score, label
            ))
        # ***************************************************************
        return results


if __name__ == "__main__":
    assert os.path.exists("yolov3_caffe_416_no_csc.om"), "you need the file 'yolov3_caffe_416_no_csc.om' in this dir"
    
    detector = YoloV3Detector(
        om_file="yolov3_caffe_416_no_csc.om"
    )

    img = cv2.cvtColor(
        cv2.imread("boat.jpg"),
        cv2.COLOR_BGR2RGB
    )

    results = detector(img)
    print(results)
    
    for result in results:
        img = cv2.rectangle(
            img,
            (result[0], result[1]), 
            (result[2], result[3]),
            (255, 0, 0), 3
        )
    
    cv2.imwrite("detected.png", 
        cv2.cvtColor(img, cv2.COLOR_BGR2RGB)
    ) 
