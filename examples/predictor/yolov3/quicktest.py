"""A simple demo of the python bindings API
with explanations

For actual detection, please use 'classtest.py'
"""

import sys, time
import numpy as np
import cv2
import valkka.acl # initializes ACL
from valkka.acl import core
import tempfile

# when using py_ascend.core directly, you need to
# call aclInit and aclFinalize manually
# this is not necessary when using py_ascend.api2 routines (see classtest.py and use_class_test.py)
with tempfile.NamedTemporaryFile() as f:
    f.write(b"{}")
    f.flush()
    core.py_aclInit(f.name) # we need the god'damn acl.json file

# TODO: type your image file here
fname = "boat.jpg"

img = cv2.imread(fname)
assert(img is not None)
img = cv2.resize(img, (416, 416))
img = cv2.cvtColor(img, cv2.COLOR_BGR2RGB)
img = img.flatten()
print("buffer type", img.dtype, "buffer shape", img.shape)

## you need to produce the .om file using the atc tool
p = core.ACLPredictor("yolov3_caffe_416_no_csc.om")

# This tells you the size of the input byte buffers
sizes = p.getInputSizes()
print("input sizes=", sizes)

# get a list of numpy arrays.  These arrays will have the results from the model execution.
# you need to call getArrayList only once
output_list = p.getArrayList()
for arr in output_list:
    print("output:", arr.shape)

print("\nWARNING: just a demo: use 'classtest.py' instead for real detection\n")
# these are bytebuffers that go into the libACL model
a1 = img # input image
a2 = np.frombuffer(np.array([
    416, 416, 416, 416
    ],dtype=np.single).tobytes(), dtype=np.uint8)

print("calling predict")
success = p.predict([a1, a2])

print("success=", success)

# results are now in these numpy arrays:
r1 = output_list[0]
r2 = output_list[1]

print("clear object")
p = None
print("sleep")
time.sleep(1)

core.py_aclFinalize()
print("bye!")

