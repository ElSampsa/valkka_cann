import cv2
import time
from classtest import YoloV3Detector 

print("instantiating models")

t=time.time()
detector1 = YoloV3Detector(
        om_file="yolov3_caffe_416_no_csc.om"
    )
print("model instantiation took", time.time()-t)

t=time.time()
detector2 = YoloV3Detector(
        om_file="yolov3_caffe_416_no_csc.om"
    )
print("model instantiation took", time.time()-t)

img = cv2.cvtColor(
        cv2.imread("boat.jpg"),
        cv2.COLOR_BGR2RGB
    )

print("using models")

t=time.time()
results = detector1(img)
print("inference took", time.time()-t)
print("1", results)

t=time.time()
results = detector2(img)
print("inference took", time.time()-t)
print("2", results)

print("bye!")
