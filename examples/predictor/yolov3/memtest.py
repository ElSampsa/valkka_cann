"""Only for debugging the python bindings.  Do not use for anything else.

::

    pip3 install --user setproctitle

"""
import setproctitle
import sys, time
import numpy as np
import cv2
import valkka.acl # initializes ACL
from valkka.acl import core
import tempfile

setproctitle.setproctitle("py_ascend")
# https://elsampsa.github.io/valkka-examples/_build/html/benchmarking.html#debugging


# when using py_ascend.core directly, you need to
# call aclInit and aclFinalize manually
# this is not necessary when using py_ascend.api2 routines (see classtest.py and use_class_test.py)
with tempfile.NamedTemporaryFile() as f:
    f.write(b"{}")
    f.flush()
    core.py_aclInit(f.name) # we need the god'damn acl.json file


fname = "boat.jpg"

img = cv2.imread(fname)
assert(img is not None)
img = cv2.resize(img, (416, 416))
img = cv2.cvtColor(img, cv2.COLOR_BGR2RGB)
img = img.flatten()
print("buffer type", img.dtype, "buffer shape", img.shape)

#memtest=1
#memtest=2
memtest=3

if memtest==1:
    # (de)instantiation loop
    for i in range(6000*2):
        print("\n", i)
        p = core.ACLPredictor("yolov3_caffe_416_no_csc.om")
        print("ok")
        time.sleep(0.01) # model instantiation takes much more time

elif memtest==2:
    # getArrayList loop
    p = core.ACLPredictor("yolov3_caffe_416_no_csc.om")
    for i in range(6000*2):
        print("\n", i)
        output_list = p.getArrayList()
        time.sleep(0.01)

else:
    p = core.ACLPredictor("yolov3_caffe_416_no_csc.om")
    a1 = img # input image
    a2 = np.frombuffer(np.array([
        416, 416, 416, 416
        ],dtype=np.single).tobytes(), dtype=np.uint8)
    for i in range(6000*2):
        print(i)
        success = p.predict([a1, a2])
        time.sleep(0.01)

print("bye!")
core.py_aclFinalize()