## YoloV3

An example on how to use the ``valkka.acl.api2.ACLPredictor`` class.

This version of yolo reads as an input, good'ol plain RGB24 images of size 416x416.

## Input

- Buffer 1 : RGB24 image : 416x416x3 = 519168 bytes
- Buffer 2 : four single-precision floats : 16 bytes. The floats represent model input width, height, original image width height

## Output

- Buffer 1 : 24576 bytes
- Buffer 2 : 32 bytes

## om File

- You have [yolov3.prototxt](yolov3.prototxt) here and need to download the model weights file [yolov3.caffemodel](https://obs-model-ascend.obs.cn-east-2.myhuaweicloud.com/yolov3/yolov3.caffemodel).
- Ascend config file [aipp_yolov3_416_no_csc.cfg](aipp_yolov3_416_no_csc.cfg) is here.

Run atc to produce the file ``yolov3_caffe_416_no_csc.om``:
```
atc --model=./yolov3.prototxt \
    --weight=./yolov3.caffemodel \
    --framework=0 \
    --output=./yolov3_caffe_416_no_csc \
    --soc_version=Ascend310 \
    --insert_op_conf=./aipp_yolov3_416_no_csc.cfg
```

## Usage

Please see [classtest.py](classtest.py) for the code example.  You can also run it directly with:
```
python3 classtest.py
```
(remember to use your system's default python version, for example "python3.6" instead of just "python")

## References

- [1](https://github.com/ChenYingpeng/caffe-yolov3) : original caffe yolov3 implementation 
