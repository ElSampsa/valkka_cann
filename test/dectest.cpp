/*
 * dectest.cpp :
 * 
* Copyright 2018
 * 
 * Authors:
 * 
 * This file is part of Valkka cpp examples
 * 
 * Valkka cpp examples is free software: you can redistribute it and/or modify
 * it under the terms of the MIT License.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 *
 */

/** 
 *  @file    dectest.cpp
 *  @author  
 *  @date    2018
 *  @version 1.0.0 
 *  
 *  @brief 
 *
 */

#include "framefifo.h"
#include "framefilter.h"
#include "livethread.h"
#include "logging.h"
#include "avdep.h"

#include "openglthread.h"
#include "avthread.h"
#include "aclthread.h"
#include "acldecoder.h"
#include "test_import.h"

using namespace std::chrono_literals;
using std::this_thread::sleep_for;

const char *stream_1 = std::getenv("VALKKA_TEST_RTSP_1");
const char *stream_2 = std::getenv("VALKKA_TEST_RTSP_2");
const char *stream_sdp = std::getenv("VALKKA_TEST_SDP");


void test_1()
{
    const char *name = "@TEST: dectest: test 1: ";
    std::cout << name << "** @@basic testing of huawei hw decoder **" << std::endl;

    // (LiveThread:livethread) --> {FrameFilter:info} --> {FifoFrameFilter:in_filter} -->> (aclthread:aclthread) --> {InfoFrameFilter:decoded_info}
    InfoFrameFilter decoded_info("decoded");
    ACLThread aclthread("aclthread", decoded_info);
    FifoFrameFilter &in_filter = aclthread.getFrameFilter(); // request framefilter from aclthread
    InfoFrameFilter out_filter("encoded", &in_filter);
    LiveThread livethread("live");

    if (!stream_1)
    {
        std::cout << name << "ERROR: missing test stream 1: set environment variable VALKKA_TEST_RTSP_1" << std::endl;
        exit(2);
    }
    std::cout << name << "** test rtsp stream 1: " << stream_1 << std::endl;

    std::cout << name << "starting threads" << std::endl;
    livethread.startCall();
    aclthread.startCall();
    sleep_for(2s);
    std::cout << name << "threads started" << std::endl;

    aclthread.decodingOnCall();

    std::cout << name << "registering stream" << std::endl;
    LiveConnectionContext ctx = LiveConnectionContext(
        LiveConnectionType::rtsp,
        std::string(stream_1),
        2,
        &out_filter); // Request livethread to write into filter info
    livethread.registerStreamCall(ctx);

    // sleep_for(1s);

    std::cout << name << "playing stream !" << std::endl;
    livethread.playStreamCall(ctx);

    sleep_for(5s);
    // sleep_for(604800s); //one week

    std::cout << name << "stopping threads" << std::endl;
    livethread.stopCall();
    // aclthread.  stopCall();
    // aclthread destructor => Thread destructor => stopCall (Thread::stopCall or aclthread::stopCall ..?) .. in destructor, virtual methods are not called
    //
}

void test_2()
{
    const char *name = "@TEST: dectest: test 2: ";
    std::cout << name << "** @@dump yuv frames from huawei hw decoder **" << std::endl;
    std::cout << name << "convert with: ffmpeg -pixel_format yuv420p -video_size 1920x1080 -i image_c10_s0_.yuv kokkelis.png" << std::endl;

    // (LiveThread:livethread) --> {FrameFilter:info} --> {FifoFrameFilter:in_filter} -->> (aclthread:aclthread) --> {InfoFrameFilter:decoded_info}
    DumpAVBitmapFrameFilter yuvdump("yuvdump");
    InfoFrameFilter decoded_info("decoded", &yuvdump);
    ACLThread aclthread("aclthread", decoded_info);
    FifoFrameFilter &in_filter = aclthread.getFrameFilter(); // request framefilter from aclthread
    InfoFrameFilter out_filter("encoded", &in_filter);
    LiveThread livethread("live");

    if (!stream_1)
    {
        std::cout << name << "ERROR: missing test stream 1: set environment variable VALKKA_TEST_RTSP_1" << std::endl;
        exit(2);
    }
    std::cout << name << "** test rtsp stream 1: " << stream_1 << std::endl;

    std::cout << name << "starting threads" << std::endl;
    livethread.startCall();
    aclthread.startCall();
    sleep_for(2s);
    std::cout << name << "threads started" << std::endl;

    aclthread.decodingOnCall();

    std::cout << name << "registering stream" << std::endl;
    LiveConnectionContext ctx = LiveConnectionContext(
        LiveConnectionType::rtsp,
        std::string(stream_1),
        2,
        &out_filter); // Request livethread to write into filter info
    livethread.registerStreamCall(ctx);

    // sleep_for(1s);

    std::cout << name << "playing stream !" << std::endl;
    livethread.playStreamCall(ctx);

    sleep_for(3s); // short time, since we're dumping raw images

    std::cout << name << "stopping threads" << std::endl;
    livethread.stopCall();
    // aclthread.  stopCall();
    // aclthread destructor => Thread destructor => stopCall (Thread::stopCall or aclthread::stopCall ..?) .. in destructor, virtual methods are not called
    //
}


void test_3()
{
    const char *name = "@TEST: dectest: test 3: ";
    std::cout << name << "** @@dump yuv frames from the normal software decoder **" << std::endl;
    std::cout << name << "convert with: ffmpeg -pixel_format yuv420p -video_size 1920x1080 -i image_c10_s0_.yuv kokkelis.png" << std::endl;

    // (LiveThread:livethread) --> {FrameFilter:info} --> {FifoFrameFilter:in_filter} -->> (aclthread:aclthread) --> {InfoFrameFilter:decoded_info}
    DumpAVBitmapFrameFilter yuvdump("yuvdump");
    InfoFrameFilter decoded_info("decoded", &yuvdump);
    // ACLThread aclthread("aclthread", decoded_info);
    AVThread aclthread("avthread", decoded_info); // let's use software decoder instead
    FifoFrameFilter &in_filter = aclthread.getFrameFilter(); // request framefilter from aclthread
    InfoFrameFilter out_filter("encoded", &in_filter);
    LiveThread livethread("live");

    if (!stream_1)
    {
        std::cout << name << "ERROR: missing test stream 1: set environment variable VALKKA_TEST_RTSP_1" << std::endl;
        exit(2);
    }
    std::cout << name << "** test rtsp stream 1: " << stream_1 << std::endl;

    std::cout << name << "starting threads" << std::endl;
    livethread.startCall();
    aclthread.startCall();
    sleep_for(2s);
    std::cout << name << "threads started" << std::endl;

    aclthread.decodingOnCall();

    std::cout << name << "registering stream" << std::endl;
    LiveConnectionContext ctx = LiveConnectionContext(
        LiveConnectionType::rtsp,
        std::string(stream_1),
        2,
        &out_filter); // Request livethread to write into filter info
    livethread.registerStreamCall(ctx);

    // sleep_for(1s);

    std::cout << name << "playing stream !" << std::endl;
    livethread.playStreamCall(ctx);

    sleep_for(3s); // short time, since we're dumping raw images

    std::cout << name << "stopping threads" << std::endl;
    livethread.stopCall();
    // aclthread.  stopCall();
    // aclthread destructor => Thread destructor => stopCall (Thread::stopCall or aclthread::stopCall ..?) .. in destructor, virtual methods are not called
    //
}


void test_4()
{
    const char *name = "@TEST: dectest: test 4: ";
    std::cout << name << "** @@DESCRIPTION **" << std::endl;


}

void test_5()
{

    const char *name = "@TEST: dectest: test 5: ";
    std::cout << name << "** @@DESCRIPTION **" << std::endl;
}

int main(int argc, char **argcv)
{
    if (argc < 2)
    {
        std::cout << argcv[0] << " needs an integer argument.  Second interger argument (optional) is verbosity" << std::endl;
    }
    else
    {

        //std::count << "trying to open acl.json file" << std::endl;
        //aclError ret = aclInit("acl.json");
        aclError ret = aclInit(NULL);
        if (ret != ACL_ERROR_NONE) {
            std::cout << "aclInit FAILED" << std::endl;
            return -1;
        }

        if (argc > 2)
        { // choose verbosity
            switch (atoi(argcv[2]))
            {
            case (0): // shut up
                ffmpeg_av_log_set_level(0);
                fatal_log_all();
                break;
            case (1): // normal
                break;
            case (2): // more verbose
                ffmpeg_av_log_set_level(100);
                debug_log_all();
                break;
            case (3): // extremely verbose
                ffmpeg_av_log_set_level(100);
                crazy_log_all();
                break;
            default:
                std::cout << "Unknown verbosity level " << atoi(argcv[2]) << std::endl;
                exit(1);
                break;
            }
        }

        switch (atoi(argcv[1]))
        { // choose test
        case (1):
            test_1();
            break;
        case (2):
            test_2();
            break;
        case (3):
            test_3();
            break;
        case (4):
            test_4();
            break;
        case (5):
            test_5();
            break;
        default:
            std::cout << "No such test " << argcv[1] << " for " << argcv[0] << std::endl;
        }

        ret = aclFinalize();
        if (ret != ACL_ERROR_NONE) {
            std::cout << "aclFinalize FAILED" << std::endl;
        }

    }
}
