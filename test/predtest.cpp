/*
 * predtest.cpp : predictor test at cpp level
 * 
 * Authors: Sampsa Riikonen <sampsa.riikonen@iki.fi>
 * 
 */

/** 
 *  @file    predtest.cpp
 *  @author  Sampsa Riikonen
 *  @date    2018
 *  @version 1.0.0 
 *  
 *  @brief   predictor test at cpp level
 *
 */

#include "valkka_acl_common.h"
#include "test_import.h"

/* // wtfffff
using namespace std::chrono_literals;
using std::this_thread::sleep_for;
*/

void test_1()
{
    // test the detector in plain c++
}

void test_2()
{
    // test the detector in python
    // TODO: instantiate your detector
    Py_Initialize();
    PyObject *array;

// stupid fix for the numpy bug:
#define import_array()                                                                    \
    {                                                                                     \
        if (_import_array() < 0)                                                          \
        {                                                                                 \
            PyErr_Print();                                                                \
            PyErr_SetString(PyExc_ImportError, "numpy.core.multiarray failed to import"); \
        }                                                                                 \
    }

    import_array();

    npy_intp dims[1];
    dims[0] = 10;
    array = PyArray_SimpleNew(1, dims, NPY_BYTE);
    // PyObject_Print(array, stdout, 0);

    //TODO: instantiate your detector
    /*
    std::cout << "\nPredict\n";
    dp.predict(array);
    
    std::cout <<"\nDecref\n";
    Py_DECREF(array);
    
    std::cout <<"\nBye\n";
    */
}

void test_3()
{
}

void test_4()
{
}

void test_5()
{
}

int main(int argc, char **argcv)
{
    if (argc < 2)
    {
        std::cout << argcv[0] << " needs an integer argument.  Second interger argument (optional) is verbosity" << std::endl;
    }
    else
    {
        //std::count << "trying to open acl.json file" << std::endl;
        //aclError ret = aclInit("acl.json");
        aclError ret = aclInit(NULL);
        if (ret != ACL_ERROR_NONE)
        {
            std::cout << "aclInit FAILED" << std::endl;
            return -1;
        }

        if (argc > 2)
        { // choose verbosity
            switch (atoi(argcv[2]))
            {
            case (0): // shut up
                // ffmpeg_av_log_set_level(0);
                // fatal_log_all();
                break;
            case (1): // normal
                break;
            case (2): // more verbose
                //  ffmpeg_av_log_set_level(100);
                //  debug_log_all();
                break;
            case (3): // extremely verbose
                // ffmpeg_av_log_set_level(100);
                // crazy_log_all();
                break;
            default:
                std::cout << "Unknown verbosity level " << atoi(argcv[2]) << std::endl;
                exit(1);
                break;
            }
        }

        switch (atoi(argcv[1]))
        { // choose test
        case (1):
            test_1();
            break;
        case (2):
            test_2();
            break;
        case (3):
            test_3();
            break;
        case (4):
            test_4();
            break;
        case (5):
            test_5();
            break;
        default:
            std::cout << "No such test " << argcv[1] << " for " << argcv[0] << std::endl;
        }

        ret = aclFinalize();
        if (ret != ACL_ERROR_NONE) {
            std::cout << "aclFinalize FAILED" << std::endl;
        }

    }
}
