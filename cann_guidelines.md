 
## CANN Installation Guidelines

Huawei R&D loves (a lot of) environmental variables to fix the directories from where
shared library and header files are found (both during compilation *and* runtime)

Instead of that, we do it like this:

- Create a softlink of the library into ``/usr/lib/ascend/``.  When your toolkit version changes, just change the softlink
- Register the shared object files into the linux shared library system - this way you don't need to set that ``LD_LIBRARY_PATH`` variable
- Our cmake files assume by defalt that .so files and headers are at ``/usr/lib/ascend/acllib``

Here are the necessary steps:
```
sudo -i
mkdir /usr/lib/ascend
ln -s /usr/local/Ascend/ascend-toolkit/latest/x86_64-linux_gcc7.3.0/acllib /usr/lib/ascend/acllib
echo "/usr/lib/ascend/acllib/lib64" > /etc/ld.so.conf.d/ascend.conf
ldconfig
exit
```

You can check that ```/usr/lib/ascend/acllib/lib64``` is on the shared library search path with
```
ldconfig -v 2>/dev/null | grep -v ^$'\t'
```

No environmental variables required anymore.

Remember to keep your ``LD_LIBRARY_PATH`` clean of any references to libacl both using compilation and runtime.
