from valkka.acl.core import py_aclInit, py_aclInitNone, py_aclFinalize
import atexit
"""
import tempfile

with tempfile.NamedTemporaryFile() as f:
    f.write(b"{}")
    f.flush()
    py_aclInit(f.name) # we need the god'damn acl.json file
"""
print("valkka.acl: ACL INIT")
py_aclInitNone()
atexit.register(py_aclFinalize)
