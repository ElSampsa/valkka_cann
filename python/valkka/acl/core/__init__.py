from .valkka_acl import * 
from .version import checkVersion, getVersion

"""TODO: do something similar here:
cuda_ok = True
checkVersion()
cuda_ok = NVcuInit()
if not cuda_ok:
    print("WARNING: valkka.nv could not be initialized:\
there's something wrong with your CUDA installation")
"""
