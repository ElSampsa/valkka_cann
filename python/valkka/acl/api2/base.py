import os
import numpy as np
from valkka.acl import core
# core.ACLPredictor is libACL C++ code with python interface


class ACLPredictor:
    """Use this to create/subclass your own model

    The API is as simple as this:

    ::

        model = Model(om_file = "path/to/file.om")
        res = model(img) # img is an image, or some other input as a numpy array

    """

    def __init__(self, om_file = None):
        """You might even want to hard-code the locations of
        the om & acl files since they are unique for each model
        """
        self.name = self.__class__.__name__
        assert(om_file is not None), "please specify om file"
        # assert(acl_file is not None), "please specify acl file"
        assert(os.path.exists(om_file)), "your om file doesn't exist"
        # assert(os.path.exists(acl_file)), "your acl file doesn't exist"

        self.om_file = om_file
        # self.acl_file = acl_file

        self.ok = False

        self.core = core.ACLPredictor(self.om_file)
        # self.core = core.PythonPredictor() # mock

        # .. that call has created everything under-the-hood:
        # libACL input & output buffers wrapped into numpy arrays, etc.
        if self.core is not None:
            self.ok = True
            self.output_buffer_list = self.core.getArrayList()
            # that list contains your libACL output byte buffers
        else:
            print(self.name, ": creating acl model failed")


    def getInputSizes(self):
        if not self.ok:
            print(self.name, ": was not initialized properly")
            return None
        else:
            return self.core.getInputSizes()


    def getOutputSizes(self):
        sizes = []
        if not self.ok:
            print(self.name, ": was not initialized properly")
            return None
        else:
            for buf in self.output_buffer_list:
                sizes.append(buf.shape[0])
        return sizes


    def __call__(self, img):
        """TODO: subclass!
        
        Transform your image (or whatever is your input), into a series
        of libACL input buffers.

        LibACL input buffers are just 1D numpy byte arrays (vectors).

        It depends completely on your model files (self.om_file & self.acl_file), how
        this transformation should be done.
        """
        raise(AssertionError("virtual class"))

        if not self.ok:
            print(self.name, ": was not initialized properly")
            return False
        # *** TRANSFORM HERE YOUR INPUT INTO A LIST OF 1D BYTEBUFFERS ***
        
        input_buffer_list = [
            img.flatten().as_type(np.uint8)
        ]
        
        # ***************************************************************
        
        success = self.core.predict(input_buffer_list) # list of input byte-buffers
        
        if not success:
            print(self.name, ": failed!")
            return False

        # if success is True, it means that self.output_buffer_list has
        # an updated list of byte buffers

        # *** TRANSFORM HERE YOUR OUTPUT BUFFER LIST ********************

        buf = self.output_buffer_list[0] # transform to float, then class names or whatever

        # ***************************************************************

        return None # return whatever your model should return :)
        