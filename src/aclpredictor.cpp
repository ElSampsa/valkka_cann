/*
 * aclpredictor.cpp : Python bindings for libACL/Ascend inference
 * 
 * Authors: Sampsa Riikonen <sampsa.riikonen@silo.ai>
 * 
 */

/** 
 *  @file    aclpredictor.cpp
 *  @author  Sampsa Riikonen
 *  @date    2020
 *  @version 0.0.1
 *  
 *  @brief   Python bindings for libACL/Ascend inference
 */ 


 /*
Analyzing the original example's codebase

ObjectDetect uses ModelProcess & DVppProcess

# Create ObjectDetect instance
ObjectDetect(kModelPath, kModelWidth, kModelHeight)
# then:
ObjectDetect.Init() =>
    self.InitResource
    self.InitModel =>
        # uses object "model_" .. a global variable (!)
        # model: loads the model from files & sends to SOC
        # creates input & output buffers
        model_.LoadModelFromFileWithMem
        model_.CreateDesc
        model_.CreateOutput
    dvpp.InitResource

ObjectDetect.Preprocess(resizedImage, image)
ret = ObjectDetect.Inference(inferenceOutput, resizedImage)
    model_.CreateInput(resizedImage.data.get()) =>
        # creates data buffer
        # sets the data into the buffer
        # etc
    # data is of type std::shared_ptr<uint8_t>
    model_.Execute() =>
        # aclmdlExecute(modelId_, input_, output_)
        # input_, etc. are object members

# ret object is of class Result
#
# this one simply marks the output image:
# ObjectDetect.Postprocess(image, inferenceOutput, imageFile)
*/

#include "aclpredictor.h"
#include "numpy_no_import.h"

// #define NO_ACL 1

PythonPredictor::PythonPredictor() : first_exec(true) {
    this->python_list = PyList_New(0);
}

PythonPredictor::~PythonPredictor() {
    if (verbose) std::cout << "PythonPredictor: dtor" << std::endl;
    freeDataBuffers();
}

void PythonPredictor::freeDataBuffers() {
    if (python_list == Py_None) {
        if (verbose) std::cout << "PythonPredictor: freeDatabuffers: quick exit" << std::endl;
        return;
    }

    if (verbose) std::cout << "PythonPredictor: freeDataBuffers" << std::endl;
    Py_DECREF(python_list);
    python_list=Py_None;

    if (verbose) std::cout << "PythonPredictor: numpy array clear" << std::endl;
    for(auto it=python_arrays.begin();it!=python_arrays.end(); it++) {
        //*it == element of the array = a pointer
        Py_DECREF(*it);
    }

    if (verbose) std::cout << "PythonPredictor: output buffer clear" << std::endl;
    for(auto it=output_byte_buffers.begin();it!=output_byte_buffers.end(); it++) {
        delete *it;
    }
    
    python_arrays.clear();
    output_byte_buffers.clear();
}

PyObject* PythonPredictor::getArrayList() {
    Py_INCREF(python_list);
    return python_list;
}

PyObject* PythonPredictor::predict(PyObject* plis) {
    Py_INCREF(plis);
    if (!PyList_Check(plis)) {
        std::cerr << "PythonPredictor: predict: requires a list of numpy arrays" << std::endl;
        Py_DECREF(plis);
        Py_RETURN_FALSE;
    }
    Py_ssize_t lislen = PyList_GET_SIZE(plis);
    Py_ssize_t i;
    std::size_t size;

    for(i=0;i<lislen;i++) { // read input buffers
        if (verbose) std::cout << "PythonPredictor: predict: getting array number " << i << std::endl;
        PyArrayObject *pa = (PyArrayObject*)
            PyList_GetItem(plis, i);

        int n_dims = PyArray_NDIM(pa);
        npy_intp *dims = PyArray_DIMS(pa);
        
        if (n_dims > 1) { // check number of dimensions : width, height, channels required
            if (verbose) std::cout << 
                "PythonPredictor: predict: list element must be a byte buffer vector" 
                << std::endl;
            Py_DECREF(plis);
            Py_RETURN_FALSE;
        }

        uint8_t* buf = (uint8_t*)PyArray_BYTES(pa); // the data byte-buffer
        size = (std::size_t)(dims[0]);
        // in this test we do nothing with the data..
    }
    
    // test: create two output buffers of length 1024
    size=1024;
    // std::cout << "PythonPredictor: 3" << std::endl;
    if (first_exec) {
        if (verbose) std::cout << "PythonPredictor: predict: first execution: will reserve" << std::endl;
        python_arrays.resize(2);
        output_byte_buffers.resize(2);
    }
    
    for(i=0;i<2;i++) { 
        if (first_exec) {
            if (verbose) std::cout << 
                "PythonPredictor: predict: creating output numpy array " << i 
                << std::endl;
            output_byte_buffers[i] = new uint8_t[size];
            //std::cout << "4" << std::endl;
            npy_intp dims[1];
            dims[0] = size;
            //std::cout << "5" << std::endl;
            python_arrays[i] = PyArray_SimpleNewFromData(
                1, dims, NPY_UBYTE, (char*)(output_byte_buffers[i])
            );
            //std::cout << "6" << std::endl;
            PyList_Append(python_list, python_arrays[i]);
        }
    }

    if (!first_exec) {
        if (verbose) std::cout << "PythonPredictor: predict: setting some values" << std::endl;
        for (auto it=output_byte_buffers.begin(); it!=output_byte_buffers.end(); ++it) {
            uint8_t* buf = *it;
            for(i=0;i<1024;i++) {
                buf[i] = 4;
            }
        }   
    }

    first_exec = false;

    Py_DECREF(plis);
    Py_RETURN_TRUE;
}

#ifdef NO_ACL
#else

ACLPredictor::ACLPredictor(const char *om_file) : PythonPredictor(), 
    activated(true), device_set(false),
    deviceId_(0), context_(NULL), stream_(NULL), // object_detect
    modelId_(0), modelMemPtr_(NULL), modelMemSize_(0),
    modelWeightPtr_(NULL), modelWeightSize_(0), modelDesc_(NULL), 
    input_(NULL), output_(NULL) // model_process
{
    
    this->om_file = std::string(om_file);
    // this->acl_file = std::string(acl_file);

    // object_detect.cpp: InitResource
    aclError ret;

    /*
    ret = aclInit(acl_file); //
    if (ret != ACL_ERROR_NONE) {
        // aclInit can only be called once _per process_ (!)
        // which makes no sense at all
        // .. what if you want detectors with different acl.json files per process?
        // printf("acl init failed");
        // acl_init = false;
        //release();
        //return;
        if (verbose) std::cout << "aclInit call failed (no problem though)" << std::endl;
    }
    if (verbose) std::cout << "acl init done" << std::endl;
    */

    // open device
    ret = aclrtSetDevice(deviceId_); //
    if (ret != ACL_ERROR_NONE) {
        printf("acl open device %d failed", deviceId_);
        release();
        return;
    }
    else {
        device_set = true;
    }
    if (verbose) std::cout << "open device success " << deviceId_ << std::endl;

    // terminate called without an active exception
    // release(); return;

    // create context (set current)
    context_ = new aclrtContext();
    ret = aclrtCreateContext(context_, deviceId_); //
    if (ret != ACL_ERROR_NONE) {
        printf("acl create context failed");
        delete context_;
        context_ = NULL;
        release();
        return;
    }
    if (verbose) std::cout << "create context success" << std::endl;

    // create stream
    stream_ = new aclrtStream();
    ret = aclrtCreateStream(stream_); //
    if (ret != ACL_ERROR_NONE) {
        printf("acl create stream failed");
        delete stream_;
        stream_ = NULL;
        release();
        return;
    }
    if (verbose) std::cout << "create stream success" << std::endl;

    // TODO: aclrtSetContext missing!?

    ret = aclrtGetRunMode(&runMode_);
    if (ret != ACL_ERROR_NONE) {
        printf("acl get run mode failed");
        release();
        return;
    }

    // ModelProcess::LoadModelFromFileWithMem

    // in pyaml there seems to be just one call: "acl.mdl.load_from_file"
    // for the next four calls:
    ret = aclmdlQuerySize(this->om_file.c_str(), &modelMemSize_, &modelWeightSize_);
    if (ret != ACL_ERROR_NONE) {
        printf("query model failed, model file is %s", this->om_file.c_str());
        release();
        return;
    }
    // 1

    ret = aclrtMalloc(&modelMemPtr_, modelMemSize_, ACL_MEM_MALLOC_HUGE_FIRST);
    if (ret != ACL_ERROR_NONE) {
        printf("malloc buffer for mem failed, require size is %zu", modelMemSize_);
        modelMemPtr_ = NULL;
        release();
        return;
    }
    // 2

    ret = aclrtMalloc(&modelWeightPtr_, modelWeightSize_, ACL_MEM_MALLOC_HUGE_FIRST);
    if (ret != ACL_ERROR_NONE) {
        printf("malloc buffer for weight failed, require size is %zu", modelWeightSize_);
        modelWeightPtr_ = NULL;
        release();
        return;
    }
    // 3

    ret = aclmdlLoadFromFileWithMem(this->om_file.c_str(), &modelId_, modelMemPtr_,
    modelMemSize_, modelWeightPtr_, modelWeightSize_);
    if (ret != ACL_ERROR_NONE) {
        printf("load model from file failed, model file is %s", this->om_file.c_str());
        release();
        return;
    }
    // 4

    // loadFlag_ = true;
    if (verbose) std::cout << "load model success " << this->om_file.c_str() << std::endl;

    // ModelProcess::CreateDesc()

    modelDesc_ = aclmdlCreateDesc(); //
    if (modelDesc_ == NULL) {
        printf("create model description failed");
        release();
        return;
    }

    ret = aclmdlGetDesc(modelDesc_, modelId_); //
    if (ret != ACL_ERROR_NONE) {
        printf("get model description failed");
        release();
        return;
    }
    if (verbose) std::cout << "model id " << modelId_ << std::endl;
    if (verbose) std::cout << "create model description success" << std::endl;

    // ModelProcess::CreateInput()
    // Q: we need to re-create the datasets always?
    input_ = aclmdlCreateDataset();
    if (input_ == NULL) {
        printf("can't create dataset, create input failed");
        release();
        return;
    }

    output_ = aclmdlCreateDataset(); // 
    if (output_ == NULL) {
        printf("can't create dataset, create output failed");
        release();
        return;
    }

    // create & keep track of input & output databuffers
    size_t size;
    size = aclmdlGetNumInputs(modelDesc_);
    // acl_input_data_buffers.resize(size);

    // [aclmdlDataset [aclDataBuffer [reserved mem]] [aclDataBuffer [reserved mem]]]
    //
    // aclGetDataBufferAdr(aclDataBuffer) => reserved mem

    for (size_t i = 0; i < size; ++i) {
        size_t buffer_size = aclmdlGetInputSizeByIndex(modelDesc_, i);

        if (verbose) std::cout << "ACLPredictor: input buffer size: " << buffer_size << std::endl;

        // reserve memory on the device
        void *buffer = nullptr;
        ret = aclrtMalloc(&buffer, buffer_size, ACL_MEM_MALLOC_NORMAL_ONLY);
        if (ret != ACL_ERROR_NONE) {
            printf("can't malloc output buffer, size is %zu", buffer_size);
            release();
            return;
        }

        // assign the reserved memory to a databuffer
        aclDataBuffer* acl_data_buffer = NULL;
        acl_data_buffer = aclCreateDataBuffer(buffer, buffer_size);
        if (!acl_data_buffer) {
            printf("can't create input data buffer");
            aclrtFree(buffer);
            release();
            return;
        }

        // assign the databuffer to a dataset
        ret = aclmdlAddDatasetBuffer(input_, acl_data_buffer);
        if (ret != ACL_ERROR_NONE) {
            printf("can't add input data buffer");
            aclrtFree(buffer);
            aclDestroyDataBuffer(acl_data_buffer);
            release();
            return;
        }

        // acl_input_data_buffers[i] = acl_data_buffer;
        acl_input_data_buffers.push_back(acl_data_buffer);
    }

    size = aclmdlGetNumOutputs(modelDesc_);
    // acl_output_data_buffers.resize(size);

    // should turn this into function f(std::vector<aclDataBuffer*>)..
    for (size_t i = 0; i < size; ++i) {
        size_t buffer_size = aclmdlGetOutputSizeByIndex(modelDesc_, i);

        if (verbose) std::cout << "ACLPredictor: output buffer size: " << buffer_size << std::endl;

        // reserve memory on the device
        void *buffer = nullptr;
        ret = aclrtMalloc(&buffer, buffer_size, ACL_MEM_MALLOC_NORMAL_ONLY);
        if (ret != ACL_ERROR_NONE) {
            printf("can't malloc output buffer");
            freeDataBuffers();
            return;
        }

        if (verbose) std::cout << "ACLPredictor: devmem reserved" << std::endl;

        // assign the reserved memory to a databuffer
        aclDataBuffer* acl_data_buffer = NULL;
        acl_data_buffer = aclCreateDataBuffer(buffer, buffer_size);
        if (!acl_data_buffer) {
            printf("can't create output data buffer");
            aclrtFree(buffer);
            release();
            return;
        }

        if (verbose) std::cout << "ACLPredictor: devmem assigned" << std::endl;

        // assign the databuffer to a dataset
        ret = aclmdlAddDatasetBuffer(output_, acl_data_buffer);
        if (ret != ACL_ERROR_NONE) {
            printf("can't add output data buffer");
            aclrtFree(buffer);
            aclDestroyDataBuffer(acl_data_buffer);
            release();
            return;
        }

        if (verbose) std::cout << "ACLPredictor: databuf assigned" << std::endl;

        // acl_output_data_buffers[i] = acl_data_buffer;
        acl_output_data_buffers.push_back(acl_data_buffer);

        // output bytes are copied from device into this buffer:
        output_byte_buffers.push_back(new uint8_t[buffer_size]);
        npy_intp dims[1];
        dims[0] = buffer_size;
        //..that buffer is exposed as a numpy byte-buffer array:
        if (verbose) std::cout << "ACLPredictor: creating numpy buffer" << std::endl;
        python_arrays.push_back(PyArray_SimpleNewFromData(
            1, dims, NPY_UBYTE, (char*)(output_byte_buffers.back())
        ));
        //..append that numpy array into a list:
        PyList_Append(python_list, python_arrays.back());
    }

    if (verbose) std::cout << "create model input & output success" << std::endl;
}


PyObject* ACLPredictor::getInputSizes() {    
    size_t size = aclmdlGetNumInputs(modelDesc_);
    PyObject* sizetup = PyTuple_New(size);
    size_t i;
    for(i=0; i<size; i++) {
        PyTuple_SET_ITEM(sizetup, i, 
            PyLong_FromSsize_t(aclmdlGetInputSizeByIndex(modelDesc_, i))
        );
    }
    return sizetup;
}

void ACLPredictor::release() {
    aclError ret;

    if (context_) {
        aclrtSetCurrentContext(*context_);
    }

    freeDataBuffers();

    if (output_) {
        ret = aclmdlDestroyDataset(output_);
        if (verbose) std::cout << "ACLPredictor: release: "
            << "output_" << std::endl;
        if (ret != ACL_ERROR_NONE) {
            printf("aclmdlDestroyDataset failed"); 
        }
        output_ = NULL;
    }

    if (input_) {
        ret = aclmdlDestroyDataset(input_);
        if (verbose) std::cout << "ACLPredictor: release: "
            << "input_" << std::endl;
        if (ret != ACL_ERROR_NONE) {
            printf("aclmdlDestroyDataset failed"); 
        }
        input_ = NULL;
    }

    if (modelDesc_) {
        aclmdlDestroyDesc(modelDesc_);
        modelDesc_ = NULL;
    }

    if (modelMemPtr_) {
        ret = aclrtFree(modelMemPtr_);
        if (verbose) std::cout << "ACLPredictor: release: "
            << "modelMemPtr_" << std::endl;
        if (ret != ACL_ERROR_NONE) {
            printf("aclrtFree failed"); 
        }
        modelMemPtr_ = NULL;
    }

    if (modelWeightPtr_) {
        ret = aclmdlUnload(modelId_);
        if (ret != ACL_ERROR_NONE) {
            printf("aclmdlUnload failed"); 
        }
        ret = aclrtFree(modelWeightPtr_);
        if (verbose) std::cout << "ACLPredictor: release: "
            << "modelWeightPtr_" << std::endl;
        if (ret != ACL_ERROR_NONE) {
            printf("aclrtFree failed"); 
        }
        modelWeightPtr_ = NULL;
    }
    
    if (stream_) {
        if (verbose) std::cout << "ACLPredictor: release: "
            << "destroy stream" << std::endl;
        ret = aclrtDestroyStream(*stream_);
        if (ret != ACL_ERROR_NONE) {
            printf("acl destroy stream failed"); 
        }
        delete stream_;
        stream_ = NULL;
    }

    if (context_) {
        ret = aclrtDestroyContext(*context_);
        if (verbose) std::cout << "ACLPredictor: release: "
            << "destroy context" << std::endl;
        if (ret != ACL_ERROR_NONE) {
            printf("acl destroy context failed"); 
        }
        delete context_;
        context_ = NULL;
    }

    if (device_set) {
        ret = aclrtResetDevice(deviceId_);
        if (verbose) std::cout << "ACLPredictor: release: "
            << "reset device" << std::endl;
        if (ret != ACL_ERROR_NONE) {
            printf("acl reset device failed"); 
        }
        device_set = false;
    }

    /*
    if (acl_init) {
        ret = aclFinalize();
        if (verbose) std::cout << "ACLPredictor: release: "
            << "finalize" << std::endl;
        if (ret != ACL_ERROR_NONE) {
            printf("acl finalize failed"); 
        }
        acl_init = false;
    }
    */

    activated = false;
}


void ACLPredictor::freeDataBuffers() {
    PythonPredictor::freeDataBuffers();
    
    /*
    std::cout << "freeing output byte buffers" << std::endl;

    for(auto it=output_byte_buffers.begin();it!=output_byte_buffers.end();++it) {
        delete(*it);
    }
    output_byte_buffers.clear();
    */

    if (verbose) std::cout << "ACLPredictor: freeDataBuffers: freeing input data buffers" << std::endl;

    for(auto it=acl_input_data_buffers.begin(); it!=acl_input_data_buffers.end(); it++) {
        void* buffer = aclGetDataBufferAddr(*it);
        aclrtFree(buffer); // release corresponding GPU memory
        aclDestroyDataBuffer(*it);
    }
    acl_input_data_buffers.clear();
    
    if (verbose) std::cout << "ACLPredictor: freeDataBuffers: freeing output data buffers" << std::endl;

    for(auto it=acl_output_data_buffers.begin(); it!=acl_output_data_buffers.end(); it++) {
        void* buffer = aclGetDataBufferAddr(*it);
        aclrtFree(buffer); // release corresponding GPU memory
        aclDestroyDataBuffer(*it);
    }
    acl_output_data_buffers.clear();
}


ACLPredictor::~ACLPredictor() {
    release();
}


PyObject* ACLPredictor::predict(PyObject* plis) {
    // plis is a list of numpy arrays
    if (!activated) {
        printf("ACLPredictor: model not active");
        Py_RETURN_FALSE;
    }

    aclrtSetCurrentContext(*context_);

    Py_INCREF(plis);
    aclError ret;
    if (!PyList_Check(plis)) {
        printf("predict requires a list of numpy arrays");
        Py_DECREF(plis);
        Py_RETURN_FALSE;
    }
    Py_ssize_t lislen = PyList_GET_SIZE(plis);

    // https://support.huawei.com/enterprise/en/doc/EDOC1100155021/5a93b435/aclmdldesc#EN-US_TOPIC_0263834171
    if (acl_input_data_buffers.size() != lislen) {
        printf("predict: incorrect number of elements in the list");
        Py_DECREF(plis);
        Py_RETURN_FALSE;
    }

    Py_ssize_t i;
    for(i=0;i<lislen;i++) { // DATABUFFER LOOP
        PyArrayObject *pa = (PyArrayObject*)
            PyList_GetItem(plis, i);

        // PyArrayObject *pa = (PyArrayObject*)po;

        int n_dims = PyArray_NDIM(pa);
        npy_intp *dims = PyArray_DIMS(pa);
        /*
        if (n_dims < 3) { // check number of dimensions : width, height, channels required
            Py_DECREF(po);
            return Py_None; // TODO
        }
        */
        // WARNING: it's up to the user to provide a raw byte buffer
        // where the bytes are in the correct order
        if (n_dims > 1) { // check number of dimensions : width, height, channels required
            printf("list element must be a byte buffer vector");
            Py_DECREF(plis);
            Py_RETURN_FALSE;
        }

        uint8_t* buf = (uint8_t*)PyArray_BYTES(pa); // the data byte-buffer
        std::size_t size = (std::size_t)(dims[0]);

        /*
        // in python, the most rapid-running index (here the color) is the last
        int h = dims[0];
        int w = dims[1];
        int c = dims[2];
        */

        // *** libACL part ***
        
        /*
        aclDataBuffer* inputData = aclCreateDataBuffer(
            buf,    // pointer to bytes in host cpu mem
            h*w*c); // number of bytes
        */
        aclDataBuffer* inputData = acl_input_data_buffers[i];
        void* memaddr = aclGetDataBufferAddr(inputData);
        
        /*
        aclCreateDataBuffer(
            buf,    // pointer to bytes in host cpu mem
            // TODO: NOPES! it is in the device memory!
            // https://support.huawei.com/enterprise/en/doc/EDOC1100155021/fd9455dd/acldatabuffer#EN-US_TOPIC_0263834157
            size
        ); // number of bytes
        */

        // TODO: need to do this
        // https://support.huawei.com/enterprise/en/doc/EDOC1100155021/ca6ef532/aclrtmemcpy

        /*
        aclError aclrtMemcpy(
            void *dst, 
            size_t destMax, 
            const void *src, 
            size_t count, 
            aclrtMemcpyKind kind)
        */

        if (verbose) std::cout << "ACLPredictor: predict: input aclrtMemcpy: " << i << std::endl;
        ret = aclrtMemcpy(
            memaddr,
            size,
            (void*)buf,
            size, 
            ACL_MEMCPY_HOST_TO_DEVICE);

        if (ret != ACL_ERROR_NONE) {
            printf("predict: uploading data from host to device failed");
            Py_DECREF(plis);
            Py_RETURN_FALSE;
        }

    } // DATABUFFER LOOP
    // ModelProcess::Execute()
    if (verbose) std::cout << "aclmdlExecute" << std::endl;
    ret = aclmdlExecute(modelId_, input_, output_);
    if (ret != ACL_ERROR_NONE) {
        printf("execute model failed, modelId is %u, reason: %i", modelId_, ret);
        //Py_DECREF(plis);
        //Py_RETURN_FALSE;
    } 
    else {
        if (verbose) std::cout << "model execute success" << std::endl;
    }

    std::size_t size;
    for (i = 0; i < acl_output_data_buffers.size(); i++) {
        if (verbose) std::cout << "ACLPredictor: predict: output aclrtMemcpy: " << i << std::endl;
        aclDataBuffer* outputData = acl_output_data_buffers[i];
        void* memaddr = aclGetDataBufferAddr(outputData);
        size = aclGetDataBufferSize(outputData);
        aclrtMemcpy(output_byte_buffers[i], size, 
            memaddr, size, 
            ACL_MEMCPY_DEVICE_TO_HOST);
    }

    if (verbose) std::cout << "ACLPredictor: predict: output data copied" << std::endl;
    
    first_exec = false;

    Py_DECREF(plis);
    Py_RETURN_TRUE;
}
#endif