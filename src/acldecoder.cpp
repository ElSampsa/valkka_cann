/*
 * acldecoder.cpp : CANN accelerated decoder class for libValkka
 *  
 * Authors:
 * 
 * (c) Copyright 2021
 * 
 *            DO WHAT THE FUCK YOU WANT TO PUBLIC LICENSE 
 *   TERMS AND CONDITIONS FOR COPYING, DISTRIBUTION AND MODIFICATION 
 * 
 *  0. You just DO WHAT THE FUCK YOU WANT TO. 
 * 
 */

/** 
 *  @file    acldecoder.cpp
 *  @author
 *  @date    2021
 *  @version 1.0.0 
 *  
 *  @brief   CANN accelerated decoder class for libValkka
 */

#include "acldecoder.h"

#define ACLDECODER_VERBOSE 1
// #define ACLDECODER_DUMP 1


void acl_decoder_callback(
    acldvppStreamDesc * input, 
    acldvppPicDesc * output, 
    void* userData) {
        ACLDecoder * acl_decoder = (ACLDecoder*)(userData);
        acl_decoder->callback(
            input, 
            output
        );
    }


/*
https://support.huawei.com/enterprise/en/doc/EDOC1100180746/41eef087/media-data-processing

*/
ACLDecoder::ACLDecoder(AVCodecID av_codec_id, int32_t device_id, int n_buf) : 
                                                                Decoder(),
                                                                device_id(device_id),
                                                                active(true),
                                                                channel_desc(NULL),
                                                                frame_config(NULL),
                                                                stream_desc(NULL),
                                                                pic_desc(NULL),
                                                                dev_ptr(NULL),
                                                                packet_mem_ptr(NULL),
                                                                semaring(n_buf),
                                                                current_width(0),
                                                                current_height(0),
                                                                en_type(H264_MAIN_LEVEL),
                                                                threadfunc_loop(true)
{

    // ringbuffer of AVBitmapFrame(s)
    int i;
    for(i=0; i<=n_buf; i++) {
        out_frame_rb.push_back(new AVBitmapFrame());
    }

    // WARNING: TODO: if something goes wrong in the initializations,
    // (1) set the corresponding object into NULL (like in the init list of this ctor)
    // (2) set this->active to false 
    // (3) and return

    /* // aclinit not here, but in the individual test programs (dectest.cpp, etc.)
    // for python it's imported at the module level
    aclError ret = aclInit(NULL);
    if (ret != 0) {
        std::cout << "ACL init failed"<<std::endl;
        return;
    }
    std::cout << "ACL init successfully"<<std::endl;
    */

    aclError ret;
    // open device
    ret = aclrtSetDevice(device_id);
    if (ret != 0) {
        std::cout << "Failed to set device " << device_id << ", ret = " << ret << std::endl;
        return;
    }
    std::cout << "Set device successfully " << device_id << std::endl;

    // create context (set current)
    ret = aclrtCreateContext(&acl_context, device_id);
    if (ret != 0) {
        std::cout << "Failed to create context for device " << device_id << ", ret = " << ret<< std::endl;
        return;
    }
    std::cout << "Create context successfully"<< std::endl;

    // create stream
    // ret = aclrtCreateStream(&acl_stream);
    // if (ret != 0) {
    //     std::cout << "Failed to create stream, ret = " << ret<< std::endl;
    //     return;
    // }
    // std::cout << "Create stream successfully" << std::endl;


    // https://support.huawei.com/enterprise/en/doc/EDOC1100155021/ecfb4280/aclvdecframeconfig
    // Place NULL for now
    // this->frame_config = aclvdecCreateFrameConfig(); //  aclvdecFrameConfig*

    // https://support.huawei.com/enterprise/en/doc/EDOC1100155021/6b635e4f/aclvdecchanneldesc#EN-US_TOPIC_0263834241
    this->channel_desc = aclvdecCreateChannelDesc(); // aclvdecChannelDesc*

    // attach information to channel descriptor
    ret = aclvdecSetChannelDescChannelId(
        this->channel_desc, // aclvdecChannelDesc*
        this->channel_id    // uint32_t // TODO: set value for channel_id
    );

    // channel descriptor needs a thread (why?)
    int err = pthread_create(
        &(this->thread_id),
        nullptr,            // thread config pars
        this->threadfunc,   // function that implements the thread
        this             // ..parameters for that thread: this very object?
    );

    ret = aclvdecSetChannelDescThreadId(
        this->channel_desc, // aclvdecChannelDesc*
        this->thread_id     // uint64_t
    );
    
    //void (* aclvdecCallback) 
    //  (acldvppStreamDesc * input, acldvppPicDesc * output, void* userData)
    ret = aclvdecSetChannelDescCallback(
        this->channel_desc, // aclvdecChannelDesc*
        acl_decoder_callback
    );

    // TODO: set en_type
    ret = aclvdecSetChannelDescEnType(
        this->channel_desc, // aclvdecChannelDesc*
        this->en_type       // acldvppStreamFormat
    );

    // Make sure that the it is YUV420SP N12
    // acldvppPixelFormat out_pic_format = PIXEL_FORMAT_YUV_SEMIPLANAR_420;
    acldvppPixelFormat out_pic_format = acldvppPixelFormat::PIXEL_FORMAT_YUV_SEMIPLANAR_420;
    ret = aclvdecSetChannelDescOutPicFormat(
        this->channel_desc, // aclvdecChannelDesc*
        out_pic_format      // acldvppPixelFormat // PIXEL_FORMAT_YUV_SEMIPLANAR_420
    );

    // finally, create the channel
    // https://support.huawei.com/enterprise/en/doc/EDOC1100155021/52e79229/channel-creation-and-destruction#EN-US_TOPIC_0263834076
    ret = aclvdecCreateChannel(
        this->channel_desc // aclvdecChannelDesc*
    );

    // *** stream descriptor for input ***
    this->stream_desc = acldvppCreateStreamDesc();

    packet_mem_size = 1024*300; // max. 300KB packet
    ret = aclrtMalloc(&packet_mem_ptr, packet_mem_size, ACL_MEM_MALLOC_HUGE_FIRST);
    ret = acldvppSetStreamDescData(this->stream_desc, packet_mem_ptr);
    
    // this is set in method pull since it depends on the input data size (right?)
    // ret = acldvppSetStreamDescSize(this->stream_desc, uint32_t size);

    /*
    enum acldvppStreamFormat {
        H265_MAIN_LEVEL = 0,
        H264_BASELINE_LEVEL,
        H264_MAIN_LEVEL,
        H264_HIGH_LEVEL
    };
    */
    // ret = acldvppSetStreamDescFormat(this->stream_desc, acldvppStreamFormat::H264_HIGH_LEVEL);
    
    // this is set in method pull:
    // ret = acldvppSetStreamDescTimestamp(this->stream_desc, uint64_t timestamp);
    // what are these...?  should they be set in method pull?
    //ret = acldvppSetStreamDescRetCode(this->stream_desc, uint32_t retCode);
    //ret = acldvppSetStreamDescEos(this->stream_desc, uint8_t eos)


    // *** picture descriptor for input **
    // TODO:
    // how can we have a picture descriptor _in the INPUT_ ??
    // the input dimensions of the image are known only after
    // it has been decoded..!
    // I guess this is the output size, etc. that is _forced_, whatever
    // the original output size might be
    // but even so, we'd like to get the original/native resolution somehow..
    // should we use the ffmpeg decoder once in order to peek this..?
    // or maybe the width / height values are overwritten
    // & different ones than defined here appear in ACLDecoder::callback ?
    this->pic_desc = acldvppCreatePicDesc();

    uint32_t width = 1920;
    uint32_t height = 1080;
    uint32_t width_stride = 1920;
    uint32_t height_stride = 1080;
    // WARNING: keep the strides equal to image dimensions

    size_t image_size = (width * height * 3)/2; // YUV420P

    ret = acldvppMalloc(&dev_ptr, image_size);
    ret = acldvppSetPicDescData(this->pic_desc, dev_ptr);
    ret = acldvppSetPicDescSize(this->pic_desc, image_size);

    // https://support.huawei.com/enterprise/en/doc/EDOC1100155021/a6954178/acldvpppixelformat
    ret = acldvppSetPicDescFormat(this->pic_desc, acldvppPixelFormat::PIXEL_FORMAT_YUV_SEMIPLANAR_420);
    ret = acldvppSetPicDescWidth(this->pic_desc, width);
    ret = acldvppSetPicDescHeight(this->pic_desc, height);
    ret = acldvppSetPicDescWidthStride(this->pic_desc, width_stride);
    ret = acldvppSetPicDescHeightStride(this->pic_desc, height_stride);
    // what is this..?
    // ret = acldvppSetPicDescRetCode(this->pic_desc,uint32_t retCode);
    //current_width = width;
    //current_height = height;
    //out_frame.reserve(width, height);
}


ACLDecoder::~ACLDecoder()
{
#ifdef ACLDECODER_VERBOSE
    std::cout << "ACLDecoder:: dtor" << std::endl;
#endif
    // WARNING: TODO:
    // check every object if they are null
    // release non-null objects correctly
    {
        std::unique_lock<std::mutex> lk(threadfunc_loop_mutex);
        threadfunc_loop = false;
    }

    void *res = nullptr;
    std::cout << "Destroy thread start." << std::endl;
    int joinThreadErr = pthread_join(thread_id, &res);
    if (joinThreadErr != 0) {
        std::cout << "Failed to join thread, threadId = " << thread_id << "err = " << joinThreadErr << std::endl;
        return;
    } else {
        if ((uint64_t)res != 0) {
            std::cout << "Failed to run thread. ret is " << (uint64_t)res << std::endl;
            return;
        }
    }
    std::cout << "Destroy thread successfully." <<std::endl;

    aclError ret;
    if (stream_desc) {
        acldvppDestroyStreamDesc(stream_desc);
        stream_desc = nullptr;
    }
    std::cout << "Destroy stream_desc successfully." <<std::endl;
    if (pic_desc) {
        acldvppDestroyPicDesc(pic_desc);
        pic_desc = nullptr;
    }
    // std::cout << "Destroy pic_desc successfully." <<std::endl;
    // if (channel_desc) {
    //     aclvdecDestroyChannel(channel_desc);
    //     aclvdecDestroyChannelDesc(channel_desc);
    //     channel_desc = nullptr;
    // }
    // std::cout << "Destroy channel_desc successfully." <<std::endl;
    // std::cout << "acl_context " << acl_context << std::endl;
    // if (acl_context != nullptr) {
    //     ret = aclrtDestroyContext(acl_context);
    //     if (ret != 0) {
    //         std::cout << "Failed to destroy context, ret = " << ret <<std::endl;
    //     }
    //     acl_context = nullptr;
    // }
    // std::cout << "Successfully destroyed context" << std::endl;

    // ret = aclrtResetDevice(device_id);
    // if (ret != 0) {
    //     std::cout << "Failed to reset device, ret = " << ret << std::endl;
    // }
    // std::cout << "Successfully reset device " << device_id;
    // TODO: etc. similar to all internal objects

    for (auto it=out_frame_rb.begin(); it!=out_frame_rb.end(); ++it) {
        delete *it;
    }
    std::cout << "End of ACLDecoder:: dtor" << std::endl;
}

void* ACLDecoder::threadfunc(void *arg) {
#ifdef ACLDECODER_VERBOSE
    std::cout << "ACLDecoder:: threadfunc start" << std::endl;
#endif
    // TODO
    // a thread that does .. something?
    // aclrtContext context = nullptr;
    // int device_id = 0;
    // aclError ret = aclrtCreateContext(&context, device_id);
    // if (ret != 0) {
    //     std::cout << "Failed to create context for device " << device_id << ", ret = " << ret<< std::endl;
    //     return reinterpret_cast<void*>(-1);
    // }
    // std::cout << "Create context successfully"<< std::endl;


    ACLDecoder *aclDecoder = (ACLDecoder *)arg;
    
    aclError ret = aclrtSetCurrentContext(aclDecoder->getContext());
    if (ret != 0) {
        std::cout << "Failed to set context, ret = " << ret << std::endl;
        return reinterpret_cast<void*>(-1);
    }
    std::cout << "Set context successfully"<< std::endl;

    std::cout << "thread start successfully" << std::endl;
    // TODO: Fix the infinite loop
    while (true) {
        // Notice: timeout 1000ms
        aclrtProcessReport(1000);
        {
            std::unique_lock<std::mutex> lk(aclDecoder->threadfunc_loop_mutex);
            if (!aclDecoder->threadfunc_loop)
            {
                break;
            }
        }
    }

    std::cout << "break from thread loop successfully" << std::endl;

    return nullptr;
}

void ACLDecoder::callback(acldvppStreamDesc * input, acldvppPicDesc * output) {
       { // PROTECTED
            aclError ret;
#ifdef ACLDECODER_VERBOSE
            std::cout << "ACLDecoder: enters callback successfully" << std::endl;
#endif
            std::unique_lock<std::mutex> lk(mutex);
            int ind = semaring.write(); // get ringbuffer index for writing the next frame
            if (ind < 0) {
                decoderlogger.log(LogLevel::normal) << "ACLDecoder: acl_decoder_callback: overflow!" << std::endl;
                return;
            }
            AVBitmapFrame *f = (AVBitmapFrame*)(out_frame_rb[ind]);
            f->copyMetaFrom(&(in_frame)); 
            // ..not correct!  in_frame might be under manipulation
            // but let's correct that later

            // memory address of the decoded image on the device
            uint8_t* memadr = (uint8_t*)acldvppGetPicDescData(output);
            uint32_t size_ = acldvppGetPicDescSize(output);
            std::cout << "acldvppGetPicDescSize successfully returns " << size_ << " bytes" << std::endl;
            // acldvppPixelFormat acldvppGetPicDescFormat(output);
            // should be YUV420P always
            uint32_t width = acldvppGetPicDescWidth(output);
            uint32_t height = acldvppGetPicDescHeight(output);
            uint32_t stride_width = acldvppGetPicDescWidthStride(output);
            uint32_t stride_height = acldvppGetPicDescHeightStride(output);
            // uint32_t acldvppGetPicDescRetCode(output); // what is this?

            // debug: 
            void *resHostBuf = nullptr;
            ret = aclrtMallocHost(&resHostBuf, size_);
            if (ret != 0) {
                std::cout << "Failed to allocate memory from host ret = " << ret << std::endl;
                return;
            }
            // Memcpy the output data from device to host
            ret = aclrtMemcpy(resHostBuf, size_, (void*)memadr,
                            size_, ACL_MEMCPY_DEVICE_TO_HOST);
            if (ret != 0) {
                std::cout << "Failed to copy memory from device to host, ret = " << ret << std::endl;
                return;
            }
            static int cnt = 1;
            std::string fileName = "./result/img.yuv." + std::to_string(cnt);
            FILE *outFileFp = fopen(fileName.c_str(), "wb+");
            if (outFileFp == nullptr) {
                std::cout << "fopen out file failed " << "img.yuv" << std::endl;
                return;
            }

            size_t writeRet = fwrite(resHostBuf, 1, size_, outFileFp);
            if (writeRet != size_) {
                std::cout << "need write " << size_ << "bytes to " << "img.yuv" << "but only write " << writeRet <<std::endl;
                fflush(outFileFp);
                fclose(outFileFp);
                return;
            }
            fflush(outFileFp);
            fclose(outFileFp);
            std::cout << "successfully write " << size_ << " bytes resHostBuf " << resHostBuf  <<std::endl;
            cnt++;

#ifdef ACLDECODER_VERBOSE
            std::cout << "ACLDecoder: callback: width: " << width <<
                " height: " << height << " stride_w: " << stride_width <<
                " stride_h: " << stride_height << std::endl;
#endif
            if ( ((int)width != current_width) || ((int)height != current_height) ) {
                std::cout << "ACLDecoder: callback: (re)reserving AVBitmapFrames" << std::endl;
                    f->reserve((int)width, (int)height); // eats int, not uint32_t
                    current_width = (int)width;
                    current_height = (int)height;

                for(auto it=out_frame_rb.begin(); it!=out_frame_rb.end(); it++) {
                    (*it)->reserve(current_width, current_height);
                }
            }
            // aclError ret;            
            uint32_t plane_size;
            // TODO
            // if width == stride_width, then
            // we can just copy whole planes
            // otherwise must iterate over lines & copy
            // only widths worth of bytes
            // those are lots of copy operations
            // in that case you might want to use asynchronous copy

            // use asynchronous copying or not
#ifdef ACLDECODER_VERBOSE
            std::cout << "ACLDecoder: starting memcopy" << std::endl;
#endif      
            // Memcpy the output data from device to host
            // resHostBuf
            // Y PLANE
            plane_size = width*height;
            ret = aclrtMemcpy(
                f->y_payload,
                plane_size,
                (void*)resHostBuf, // memory address in the device
                plane_size, // max allowed size
                ACL_MEMCPY_HOST_TO_HOST
            );

            if(ret != 0){
#ifdef ACLDECODER_VERBOSE
                std::cout << "Failed to copy Y channnel to y_payload with ret: " << ret << std::endl;
#endif 
                return;
            }else{
                std::cout << "Successfully copied Y channnel to y_payload" << std::endl;
            }

            uint8_t* uv_channel_start = ((uint8_t*)resHostBuf) + plane_size;
            uint32_t no_bytes_left = plane_size / 2;
            uint32_t uv_payload_idx = 0;
            for (uint32_t i=0; i < no_bytes_left; i=i+2){
                uv_payload_idx = i / 2;
                f->u_payload[uv_payload_idx] = uv_channel_start[i];
                f->v_payload[uv_payload_idx] = uv_channel_start[i+1];
            }

            std::cout << "Successfully copied U, V channnels to u_payload and v_payload" << std::endl;
            // TODO: allocate it in the constructor
            // 4096 x 4096 is the largest image resolution VDEC supports
            aclrtFreeHost(resHostBuf);
            // if you used asynchronous copy, be sure
            // that you synchronize here!

        } // PROTECTED
    }


void ACLDecoder::deactivate(const char *err)
{
    std::cout << "ACLDecoder: deactivated: " << err << std::endl;
    this->active = false;
}

bool ACLDecoder::isOk()
{
    return this->active;
}

void ACLDecoder::flush()
{
    if (!active)
    {
        return;
    }
    { // PROTECTED
        std::unique_lock<std::mutex> lk(this->mutex);
        semaring.reset();
    } // PROTECTED
}

Frame *ACLDecoder::output()
{
    if (!active)
    {
        return NULL;
    }
    { // PROTECTED
        std::unique_lock<std::mutex> lk(this->mutex);
        int ind = semaring.getIndex();
        if (ind < 0)
        {
            return NULL;
        }
    #ifdef ACLDECODER_VERBOSE
        std::cout << "ACLDecoder: output: returning index " << ind << std::endl;
    #endif
        return out_frame_rb[ind];
    } // PROTECTED
}

void ACLDecoder::releaseOutput()
{
    if (!active)
    {
        return;
    }
    { // PROTECTED
        std::unique_lock<std::mutex> lk(this->mutex);
        int ind = semaring.read();
    }
}


bool ACLDecoder::pull()
{
    if (!active)
    {
        return false;
    }
#ifdef ACLDECODER_VERBOSE
    std::cout << "ACLDecoder: pull" << std::endl;
#endif
    aclError ret;
    // prepare input stream descriptor somewhat
    // only god knows in which units this timestamp is
    // PTS = presentation timestamp..?
    uint64_t timestamp = in_frame.mstimestamp; // need to transform?
    ret = acldvppSetStreamDescTimestamp(this->stream_desc, timestamp);

    // set the data length
    uint32_t size_ = (uint32_t)in_frame.payload.size();
    ret = acldvppSetStreamDescSize(this->stream_desc, size_);
    // copy data from in_frame.payload to device memory
    ret = aclrtMemcpy(
        packet_mem_ptr,
        packet_mem_size, 
        (void*)in_frame.payload.data(),
        in_frame.payload.size(),
        ACL_MEMCPY_HOST_TO_DEVICE
    );

    // https://support.huawei.com/enterprise/en/doc/EDOC1100155021/6b635e4f/aclvdecchanneldesc#EN-US_TOPIC_0263834241
    ret = aclvdecSendFrame(
        channel_desc, 
        stream_desc,
        pic_desc,
        frame_config,
        this
        );
    // 0 on success; else, failure

    /*
    if (first_timestamp == 0)
    {
        first_timestamp = in_frame.mstimestamp;
    }
    packet.timestamp = (in_frame.mstimestamp - first_timestamp) * 10000; // "10Mhz clock"
    */

#ifdef ACLDECODER_VERBOSE
    std::cout << "timestamp: " << timestamp << std::endl;
#endif
    { // PROTECTED
        // check if there is stuff in the ringbuffer
        std::unique_lock<std::mutex> lk(this->mutex);
        if (semaring.isEmpty())
        {
#ifdef ACLDECODER_VERBOSE
            std::cout << "ACLDecoder: pull returning false" << std::endl;
#endif
            return false;
        }
        else
        {
#ifdef ACLDECODER_VERBOSE
            std::cout << "ACLDecoder: pull returning true" << std::endl;
#endif
            return true;
            // The decoder thread calls ACLDecoder::output() immediately after calling this method
        }
    } // PROTECTED
}


