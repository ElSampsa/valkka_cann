/*
 * aclthread.cpp : Cuda accelerated decoder thread for CANN
 * 
 * Authors:
 *
 * (c) Copyright 2021
 * 
 *            DO WHAT THE FUCK YOU WANT TO PUBLIC LICENSE 
 *   TERMS AND CONDITIONS FOR COPYING, DISTRIBUTION AND MODIFICATION 
 * 
 *  0. You just DO WHAT THE FUCK YOU WANT TO. 
 * 
 */


/** 
 *  @file    aclthread.cpp
 *  @author  
 *  @date    2021
 *  @version 1.0.0 
 *  
 *  @brief   Cuda accelerated decoder thread for CANN
 */ 

#include "aclthread.h"
#include "acldecoder.h"

ACLThread::ACLThread(const char* name, FrameFilter& outfilter, FrameFifoContext fifo_ctx) 
    : DecoderThread(name, outfilter, fifo_ctx)
    {
    }

ACLThread::~ACLThread() {

}

Decoder* ACLThread::chooseAudioDecoder(AVCodecID codec_id) {
    DecoderThread::chooseAudioDecoder(codec_id);
}

Decoder* ACLThread::chooseVideoDecoder(AVCodecID codec_id) {
    //TODO: try to get ACLDecoder, if it's not possible, default
    //to AVDecoder
    switch (codec_id) { // switch: video codecs
        case AV_CODEC_ID_H264:
            std::cout << "Obtained ACLDecoder successfully" << std::endl;
            return new ACLDecoder(AV_CODEC_ID_H264);
            break;
        default:
            return NULL;
            break;        
    }
}

Decoder* ACLThread::fallbackAudioDecoder(AVCodecID codec_id) {
    return DecoderThread::chooseAudioDecoder(codec_id);
}


Decoder* ACLThread::fallbackVideoDecoder(AVCodecID codec_id) {
    return DecoderThread::chooseVideoDecoder(codec_id);
}



DumpAVBitmapFrameFilter::DumpAVBitmapFrameFilter(const char *name, FrameFilter *next) : FrameFilter(name, next), count(0)
{
}

void DumpAVBitmapFrameFilter::go(Frame *frame)
{ // du -h *.bin | grep "s0"
    // std::cout << "DumpFrameFilter: writing "<< frame->payload.size() << " bytes" << std::endl;
    // std::cout << "DumpFrameFilter: payload : " << frame->dumpPayload() <<std::endl;
    // std::ofstream fout(filename, std::ios::out | std::ofstream::binary);
    /*
    for(auto it=(frame->payload).begin(); it!=(frame->payload).end(); ++it) {
        std::cout << ">>" << (int)*it;
        fout << (char)*it;
    }
    */
    // frame->dumpPayloadToFile(fout);
    // std::copy((frame->payload).begin(), (frame->payload).end(), std::ostreambuf_iterator<char>(fout));

    if (frame->getFrameClass() != FrameClass::avbitmap)
    {
        decoderlogger.log(LogLevel::debug) << "DumpAVBitmapFrameFilter: go: ERROR: frame must be AVBitmapFrame " << *frame << std::endl;
        return;
    }
    AVBitmapFrame *f = static_cast<AVBitmapFrame *>(frame);

    if (f->y_payload == NULL) {
      std::cout << "DumpAVBitmapFrameFilter: Empty AVBitmapFrame" << std::endl;
      return;
    }
    std::string filename = std::string("image_c") + // std::string(f->bmpars.y_linesize) + std::string("x") + std::string(f->height) +
                        std::to_string(count) + std::string("_s") +                  // packet count
                        std::to_string(f->subsession_index) + std::string("_") + // subsession index
                        // std::to_string(frame->mstimestamp) +                         // timestamp
                        std::string(".yuv");

    std::ofstream fout(filename, std::ios::out | std::ofstream::binary);

    int i;
    for(i=0; i<f->bmpars.y_size; i++) {
        fout << f->y_payload[i];
    }
    for(i=0; i<f->bmpars.u_size; i++) {
        fout << f->u_payload[i];
    }
    for(i=0; i<f->bmpars.v_size; i++) {
        fout << f->v_payload[i];
    }
    fout.close();
    std::cout << "DumpAVBitmapFrameFilter: wrote YUV420P image " << count << " : " << f->bmpars.y_linesize << "x" << f->bmpars.height << std::endl;
    count++;
}

